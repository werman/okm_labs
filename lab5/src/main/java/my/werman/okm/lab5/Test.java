package my.werman.okm.lab5;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void main(String[] args) {

        String hostname;
        try
        {
            InetAddress addr;
            addr = InetAddress.getLocalHost();
            hostname = addr.getHostName();
        }
        catch (UnknownHostException ex)
        {
            System.out.println("Hostname can not be resolved");
            return;
        }

        Server server = new Server();
        try {
            server.start(1400);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<Client> clients = new ArrayList<>();

        for (int i = 0; i < 100000; i++) {

            Client client = new Client();
            try {
                client.start(hostname, 1400);
                client.send("Init");
            } catch (IOException e) {

                e.printStackTrace();
                System.out.printf("Failed to create connection on %d connection - abort.\n", i);
                break;
            }

            clients.add(client);

            boolean dead = false;
            if (i % 40 == 0) {
                dead = clients.stream().anyMatch(c -> (!c.send("t") || !c.connection.isAlive()));
            }


            if (dead) {
                System.out.printf("Failed to create connection on %d connection - abort.\n", i);
                break;
            }

            if (i % 50 == 0) {
                System.out.printf("Connections count %d\n", i);
            }

            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

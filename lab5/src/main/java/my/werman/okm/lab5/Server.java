package my.werman.okm.lab5;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Server {

    private DatagramSocket socket;

    private Thread clientReaderThread;

    private Lock connectionsLock;
    private Set<String> connections = new HashSet<>();

    public void start(int serverPort) throws IOException {
        socket = new DatagramSocket(serverPort);
        socket.setReceiveBufferSize(1638400);

        connectionsLock = new ReentrantLock();

        clientReaderThread = new Thread(this::clientDataListener);
        clientReaderThread.setDaemon(true);
        clientReaderThread.start();
    }

    private void clientDataListener() {
        byte[] buffer = new byte[65536];
        DatagramPacket incoming = new DatagramPacket(buffer, buffer.length);

        while (true) {
            try {
                socket.receive(incoming);
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }

            String sender = incoming.getAddress().getHostAddress() + " : " + incoming.getPort();

            if (!connections.contains(sender)) {
                System.out.printf("Client connected %s\n", sender);
                connections.add(sender);
            }

            byte[] data = incoming.getData();
            String s = new String(data, 0, incoming.getLength());
            try {
                Thread.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

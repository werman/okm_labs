package my.werman.okm.lab5;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class ClientConnection {

    private DatagramSocket socket;

    public ClientConnection(DatagramSocket socket) {
        this.socket = socket;
    }

    public boolean isAlive() {
        return socket.isConnected() && socket.isBound();
    }

    public boolean send(String str) {
        try {
            byte[] message = str.getBytes();
            DatagramPacket packet = new DatagramPacket(message, message.length);
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}

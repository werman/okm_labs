package my.werman.okm.lab5;


import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Client {

    private DatagramSocket socket;

    ClientConnection connection;

    public void start(String host, int port) throws IOException {
        InetAddress address = InetAddress.getByName(host);
        socket = new DatagramSocket();
        socket.setSendBufferSize(1638400);
        socket.setReceiveBufferSize(1638400);
        socket.connect(address, port);

        connection = new ClientConnection(socket);
    }

    public boolean send(String str) {
        return connection.send(str);
    }
}
